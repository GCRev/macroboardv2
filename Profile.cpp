#include "Profile.h"
#include "Modal.h"

BasicKey::BasicKey(int key, byte mod, int advance, BasicKey* next) {
	this->key = key;
	this->mod = mod;
	this->advance = advance;
	this->next = next;
	next->previous = this;
}

Key::Key(int key, byte mod) {
	defaultKey = new BasicKey(key, mod);
	this->currentKey = defaultKey;
	cacheSequenceLength();
}

Key::Key(int key, byte mod, const char* title) {
	defaultKey = new BasicKey(key, mod);
	this->currentKey = defaultKey;
	this->title = title;
	cacheSequenceLength();
}

Key::Key(BasicKey* key, const char* title) {
	this->title = title;
	defaultKey = key;
	currentKey = defaultKey;
	cacheSequenceLength();
}

Key::Key(BasicKey* key, const char* title, bool repeat) {
	this->title = title;
	defaultKey = key;
	currentKey = defaultKey;
	this->repeat = repeat;
	cacheSequenceLength();
}

Key::Key(BasicKey* key, const char* title, Key* resetKey) {
	this->title = title;
	defaultKey = key;
	currentKey = defaultKey;
	resetTargetKey = resetKey;
	cacheSequenceLength();
}

void Key::resetTargetKeyReset() {
	if (resetTargetKey != 0) {
		resetTargetKey->resetKey();
	}
}

Key* Key::setTitle(const char* title) {
	this->title = title;
	return this;
}

Key* Key::setRepeat(bool repeat) {
	this->repeat = repeat;
	return this;
}

Key* Key::setResetKey(Key* targ) {
	resetTargetKey = targ;
	return this;
}

int Key::getIndex() {
	return currentIndex;
}

Key* Key::addNextKey(BasicKey* nextKey) {
	BasicKey* curr = defaultKey;
	while (curr->next != nullptr) {
		curr = curr->next;
	}
	curr->next = nextKey;
	return this;
}

bool Key::advanceKey() {
	bool adv = currentKey->next != nullptr;
	if (currentIndex < sequenceLength) {
		currentIndex++;
	}
	else if (repeat) {
		currentIndex = 1;
	}

	if (currentKey->next != nullptr) {
		currentKey = currentKey->next;
	}
	else if (repeat) {
		currentKey = defaultKey;
	}

	return adv;
}

BasicKey* Key::getNext(bool wrap) {
	BasicKey* curr = currentKey;
	if (currentKey->next != nullptr) {
		curr = currentKey->next;
	}
	else if (repeat || wrap) {
		curr = defaultKey;
	}
	return curr;
}

BasicKey* Key::getPrevious(bool wrap) {
	BasicKey* curr = currentKey;
	if (currentKey->previous != nullptr) {
		curr = currentKey->previous;
	}
	else if (repeat || wrap) {
		curr = lastKey;
	}
	/*Mode::disp->setTextColor(WHITE);
	Mode::disp->clearDisplay();
	Mode::disp->setCursor(Mode::disp->width() / 4 - (5 * 5) / 2, Mode::disp->height() - 21);
	Mode::disp->print(curr->key);
	Mode::disp->display();
	delay(1000);*/
	return curr;
}

BasicKey* Key::getDefaultKey() {
	return defaultKey;
}

BasicKey* Key::getLastKey() {
	if (lastKey == nullptr) {
		cacheSequenceLength();
	}
	return lastKey;
}

BasicKey* Key::getKey() {
	return currentKey;
}

void Key::cacheSequenceLength() {
	int length = 1;
	if (defaultKey != nullptr) {
		BasicKey* curr = defaultKey;
		while (curr->next != nullptr) {
			length++;
			curr = curr->next;
		}
		lastKey = curr;
	}
	sequenceLength = length;
}

void Key::resetKey() {
	currentKey = defaultKey;
	currentIndex = 0;
}

Key* Key::setAlwaysReset(bool alwaysReset) {
	this->alwaysReset = alwaysReset;
	return this;
}

bool Key::getAlwaysReset() {
	return alwaysReset;
}

Profile::Profile(const char * title, int hue, Key** lut, int dimx, int dimy)
{
	this->title = title;
	this->lut = lut;
	this->dimx = dimx;
	this->dimy = dimy;
	this->col = hue;
}

const char * Profile::getTitle()
{
	return title;
}

Key * Profile::getKey(int x, int y)
{
	// dimx =  7
	// dimy = 5
	// lut.length = dimx*dimy [0-34]
	if (x >= dimx) {
		x = dimx - 1;
	}
	if (y >= dimy) {
		y = dimy - 1;
	}
	Key* result = lut[dimx * y + x];
	return result;
}

Key * Profile::getExtra(int i)
{
	return lut[dimx*dimy + i];
}

int Profile::getDimy()
{
	return dimy;
}

int Profile::getDimx()
{
	return dimx;
}

int Profile::getR()
{
	return 0xFF & col >> 16;
}

int Profile::getG()
{
	return 0xFF & col >> 8;
}

int Profile::getB()
{
	return 0xFF & col;
}

void KritaRotaryKnobKey::keyReleased() {
	switchTarget->state = 1 - switchTarget->state;
}

bool KritaBrushSizeKey::advanceKey() {
	
	bool result = false;
	if (switchTarget != nullptr &&
		!switchTarget->state) {
		currentIndex = 1;
		currentKey = defaultKey;
	}
	else {
		result = Key::advanceKey();
	}
	return result;
}