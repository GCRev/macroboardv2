#include "Modal.h"
#include "CharOps.h"


Mode::Mode(const char * title)
{
	this->title = title;

}

Mode::Mode(const char * title, Mode ** subs, int subsLen) : Mode(title)
{
	numChildren = subsLen;
	for (int a = 0; a < subsLen; a++)
	{
		subs[a]->setParent(this);
	}

	children = subs;
}

int Mode::cycle(int i) {

	if (hasChildren()) {
		selected += i;
		if (selected < 0) {
			selected = numChildren - 1;
		}
		else if (selected > numChildren - 1) {
			selected = 0;
		}
		setUpdate(true);
	}
	return selected;

}

void Mode::onSelect()
{
}

void Mode::onExit()
{
	// do nothing;
}

int Mode::getSelected() {
	return selected;
}

void Mode::setSelected(int i)
{
	selected = i;
}

Mode * Mode::getParent()
{
	return parent;
}

Mode * Mode::getChild(int i)
{
	return children[i];
}

void Mode::setParent(Mode * par)
{
	parent = par;
}

bool Mode::hasChildren()
{
	return numChildren > 0;
}

bool Mode::hasParent()
{
	return parent != 0;
}

void Mode::setChildren(Mode ** subs, int len)
{
	children = subs;
	numChildren = len;
}

int Mode::childCount() {

	return numChildren;

}

void Mode::execute()
{
	// do nothing;
}

void Mode::onDisplay(Adafruit_SSD1306* disp)
{
	// do nothing;
}

bool Mode::needsUpdate()
{
	return needs_update;
}

void Mode::setUpdate(bool flag)
{
	needs_update = flag;
}

bool Mode::isOperator()
{
	return false;
}

Profile * Mode::getProfile()
{
	return 0;
}

void Mode::switchState(int sw, DBounce* state) {}

OperateMode::OperateMode(Profile * profile) : Mode(profile->getTitle())
{
	this->current_profile = profile;
}

void OperateMode::sendKey(Key* k, bool rise)
{
	sendKey(k, rise, false);
}
void OperateMode::sendKey(Key* k, bool rise, bool sequence)
{
	BasicKey* targ = k->getKey();
	// -rise is a two's complement way of filling
	// the entire value with 1's if the boolean is true
	// true == 1; -true == -1;
	// -1 is two's complement for all bits set

	// if mod == 011 and rise == true
	// then (-rise ^ mod) == 1111 1100
	// & the bitmask 1<<2 == 0000 0100
	// ^= with existing mod == 0000 0111

	// if mod == 011 and rise == false
	// then (-rise ^ mod) == 0000 0011
	// & the bitmask 1<<2 == 0000 0000
	// ^= with existing mod == 0000 0011

	//if mod == 101 and rise == false
	// then (-rise ^ mod) == 0000 0101
	// & the bitmask 1<<2 == 0000 0100
	// ^= with existing mod == 0000 0001
	if (targ != nullptr) {
		if (targ->key == KEY_LEFT_SHIFT) {
			mod ^= (-rise ^ mod) & (1 << 2);
		}
		if (targ->key == KEY_LEFT_CTRL) {
			mod ^= (-rise ^ mod) & (1 << 1);
		}
		if (targ->key == KEY_LEFT_ALT) {
			mod ^= (-rise ^ mod) & (1 << 0);
		}
	}
	if (!rise) {
		Keyboard.releaseAll();
		k->keyReleased();
	}
	if (rise) {

		k->resetTargetKeyReset();
		if (targ != nullptr) {
			unsigned char local_mod = mod;
			if (sequence) {
				local_mod = 0b000;
			}
			if (!preview_mode) {
				if (targ->mod >> 2 & 0b1 || local_mod >> 2 & 0b1) {
					Keyboard.press(KEY_LEFT_SHIFT);
					//s += " + SHIFT";
				}
				if (targ->mod >> 1 & 0b1 || local_mod >> 1 & 0b1) {
					Keyboard.press(KEY_LEFT_CTRL);
					//s += " + CTRL";
				}
				if (targ->mod >> 0 & 0b1 || local_mod >> 0 & 0b1) {
					Keyboard.press(KEY_LEFT_ALT);
					//s += " + ALT";
				}
				if (targ->key >= 0) {
					Keyboard.press(targ->key);
				}
			}
			if (k->advanceKey()) {
				Keyboard.releaseAll();
				if (targ->advance >= 0) {
					delay(targ->advance);
					sendKey(k, true, true);
				}
			}
		}
		k->keyPressed();
	}
}

void OperateMode::switchState(int sw, DBounce* state) {
	if (current_profile != 0) {
		if ((sw == 2 || sw == 7) && state->state > 0) {
			digitalWrite(sw_ledPins[2], 1);
			delay(100);
			digitalWrite(sw_ledPins[2], 0);
			delay(50);
			digitalWrite(sw_ledPins[2], 1);
			delay(100);
			digitalWrite(sw_ledPins[2], 0);
			state->state = 0;
			int dimx = current_profile->getDimx();
			int dimy = current_profile->getDimy();
			for (int y = 0; y < dimy; y++) {
				for (int x = 0; x < dimx; x++) {
					current_profile->getKey(x, y)->resetKey();
				}
			}
			for (int a = 0; a < 3; a++)
			{
				current_profile->getExtra(a)->resetKey();
			}
			setUpdate(true);
		}
		if (sw == 5) {
			setUpdate(true);
		}
	}
}

void OperateMode::execute() {

	if (current_profile != 0) {
		preview_mode = digitalRead(sw_ledPins[5]);
		int dimx = current_profile->getDimx();
		int dimy = current_profile->getDimy();
		//int dimx = 7;
		//int dimy = 5;
		bool found = false;
		for (int y = 0; y < dimy; y++) {
			if (y == 0) {
				digitalWrite(rowPins[dimy - 1], HIGH);
				digitalWrite(rowPins[0], LOW);
			}
			else {
				digitalWrite(rowPins[y - 1], HIGH);
				digitalWrite(rowPins[y], LOW);
			}
			for (int a = 0; a < dimx; a++) {
				//buttons[a]->update();

				Key* k = current_profile->getKey(a, y);
				found |= !digitalRead(colPins[a]);
				//k->state = !digitalRead(colPins[a]);
				if ((!digitalRead(colPins[a]) && k->bounce == 0) || (k->bounce > 0 && k->bounce < bounceCount)) {
					k->bounce++;
				}
				if (k->bounce == bounceCount) {
					if (!digitalRead(colPins[a])) {
						if (last_key != nullptr && last_key->getAlwaysReset() && last_key != k) {
							last_key->resetKey();
						}
						last_key = k;
						sendKey(k, true);
						setUpdate(true);
					}
					k->bounce++;
				}
				if (k->bounce > bounceCount) {
					if (digitalRead(colPins[a])) {
						k->bounce = 0;
						k->state = 0;
						sendKey(k, false);
						setUpdate(true);
					}
				}
			}
		}
		digitalWrite(13, found);
	}
}

int OperateMode::cycle(int i)
{
	if (current_profile != 0) {
		if (i > 0) {
			last_key = current_profile->getExtra(0);
			sendKey(last_key, true);
		}
		if (i < 0) {
			last_key = current_profile->getExtra(1);
			sendKey(last_key, true);
		}
		sendKey(current_profile->getExtra(0), false);
		setUpdate(true);
	}
	return 0;
}

void OperateMode::onDisplay(Adafruit_SSD1306* disp)
{

	disp->setTextColor(WHITE);
	disp->setTextSize(1);
	disp->setCursor(disp->width() / 4 - (5 * 5) / 2, disp->height() - 17);
	disp->print("SHIFT");
	disp->setCursor(disp->width() / 2 - (4 * 5) / 2 + 4, disp->height() - 17);
	disp->print("CTRL");
	disp->setCursor(disp->width() * 0.75 - (3 * 5) / 2, disp->height() - 17);
	disp->print("ALT");
	int local_mod = 0;
	if (last_key != nullptr) {
		local_mod = last_key->getPrevious(true)->mod;
	}
	if ((mod | local_mod) >> 2 & 0b1) {
		disp->drawRect(disp->width() / 4 - (5 * 5) / 2 - 2, disp->height() - 19, 7 * 5 - 2, 11, WHITE);
	}

	if ((mod | local_mod) >> 1 & 0b1) {
		disp->drawRect(disp->width() / 2 - (4 * 5) / 2 + 2, disp->height() - 19, 5 * 5 + 2, 11, WHITE);
	}

	if ((mod | local_mod) >> 0 & 0b1) {
		disp->drawRect(disp->width() * 0.75 - (3 * 5) / 2 - 2, disp->height() - 19, 4 * 5 + 1, 11, WHITE);
	}
	disp->setTextColor(WHITE);
	BasicKey* currKey = last_key->getKey();
	if (last_key != nullptr) {
		if (currKey != nullptr &&
			currKey->key != KEY_LEFT_SHIFT &&
			currKey->key != KEY_LEFT_CTRL &&
			currKey->key != KEY_LEFT_ALT) {
			//if (isAlphaNum(last_key->key)) {
			BasicKey* curr = last_key->getDefaultKey();
			int numKeys = last_key->sequenceLength;
			int count = 0;
			disp->drawFastHLine(0, disp->height() - 25, disp->width(), BLACK);
			while (curr != nullptr) {
				disp->setCursor((disp->width() - 10 * numKeys) / 2 + count * 10, disp->height() - 35);
				int tempKey = curr->key;
				if (curr->key == -1) {
					tempKey = 0x09;
				}
				disp->print((char)toUpper(tempKey));

				if (count < last_key->getIndex()) {
					disp->drawPixel((disp->width() - 10 * numKeys) / 2 + count * 10 + 1, disp->height() - 25, WHITE);
					disp->drawPixel((disp->width() - 10 * numKeys) / 2 + count * 10 + 2, disp->height() - 25, WHITE);
					disp->drawPixel((disp->width() - 10 * numKeys) / 2 + count * 10 + 3, disp->height() - 25, WHITE);
				}
				count++;
				curr = curr->next;

			}
		}
		//}
		if (last_key->title != nullptr) {
			size_t len = strlen(last_key->title);
			disp->setCursor((disp->width() - (5 * len)) / 2 - 2, disp->height() - 46);
			disp->setTextSize(1);
			disp->print(last_key->title);
		}
	}
	if (preview_mode) {
		disp->setCursor(disp->width() - 7, 1);
		disp->print((char)0xEE);
		disp->setCursor(disp->width() - 7, 0);
		disp->print((char)0xDB);
		disp->drawPixel(disp->width() - 5, 5, BLACK);
		disp->drawPixel(disp->width() - 5, 6, BLACK);
	}
}

void OperateMode::setCurrentProfile(Profile * prof)
{
	current_profile = prof;
}

void OperateMode::onSelect()
{
	if (!begin) {
		begin = true;
	}
	else {
		if (current_profile != 0) {
			last_key = current_profile->getExtra(2);
			sendKey(last_key, true);
			sendKey(last_key, false);
			setUpdate(true);
		}
	}
}

void OperateMode::onExit() {
	begin = false;
}

bool OperateMode::isOperator()
{
	return true;
}

Profile * OperateMode::getProfile()
{
	return current_profile;
}

ProfileMode::ProfileMode(Profile * prof) : Mode(prof->getTitle())
{
	target_profile = prof;
}

SettingsMode::SettingsMode() : Mode("Settings")
{
}

void SettingsMode::onDisplay(Adafruit_SSD1306 * disp)
{
	disp->setTextColor(WHITE);
	disp->setTextSize(1);
	printDirectory(root, disp);

}

void SettingsMode::execute()
{
	//root.close();
}


void SettingsMode::onSelect()
{
	root = SD.open("/");
	root.rewindDirectory();
}

int SettingsMode::cycle(int i)
{
	if (hasChildren() || numFiles > 0) {
		fileIndex += i;
		if (fileIndex < 0) {
			fileIndex = numFiles - 1;
		}
		else if (fileIndex > numFiles - 1) {
			fileIndex = 0;
		}
		setUpdate(true);
	}
	return fileIndex;
}

void SettingsMode::printDirectory(File dir, Adafruit_SSD1306* disp)
{
	numFiles = 0;
	disp->fillRect(0, 10 + fileIndex * 10, disp->width(), 11, WHITE);
	while (true) {

		File entry = dir.openNextFile();
		if (!entry) {
			// no more files
			dir.rewindDirectory();
			break;
		}
		if (!entry.isDirectory()) {
			if (numFiles - startIndex < maxFiles) {
				if (numFiles == fileIndex) {
					disp->setTextColor(BLACK, WHITE);
				}
				else {
					disp->setTextColor(WHITE);
				}
				disp->setCursor(4, 12 + (10 * numFiles));
				disp->print(entry.name());
			}
			numFiles++;
		}
		entry.close();
	}
	/*disp->setCursor(4, 12 + (10 * numFiles));
	disp->print(numFiles);*/

}

ProfileLoaderMode::ProfileLoaderMode(File f) : Mode("Profile")
{

}
