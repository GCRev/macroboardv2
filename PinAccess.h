#pragma once

const unsigned char sw_ledPins[6] = {23, 22, 21, 20, 19, 18};
const unsigned char but_ledPins[3] = {17, 16, 39};
const unsigned char colPins[7] = {4, 5, 6, 24, 25, 26, 27};
const unsigned char rowPins[5] = {28, 29, 30, 31, 32};
const unsigned char sw_pins[8] = {38, 37, 36, 35, 34, 33, 2, 7};
