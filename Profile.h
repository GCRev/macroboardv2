#pragma once
typedef unsigned char byte;

struct DBounce {
	DBounce(int bounce, byte state) : bounce(bounce), state(state) {};
	int bounce;
	byte state;
	byte block = 0;
};

struct BasicKey {
	int key = 0;
	byte mod = 0b000;
	int advance = -1;
	BasicKey* next = nullptr;
	BasicKey* previous = nullptr;
	BasicKey() {};
	BasicKey(int key, byte mod) :key(key), mod(mod), advance(-1) {};
	BasicKey(int key, byte mod, int advance) :key(key), mod(mod), advance(advance) {};
	BasicKey(int key, byte mod, int advance, BasicKey* next);

	//BasicKey* previous = nullptr;
};

class Key {
public:
	Key(int key, byte mod);
	Key(int key, byte mod, const char* title);
	Key(BasicKey* key, const char* title);
	Key(BasicKey* key, const char* title, bool repeat);
	Key(BasicKey* key, const char* title, Key* resetKey);
	Key(const char* title);

	Key* setRepeat(bool repeat);
	Key* setTitle(const char* title);
	Key* addNextKey(BasicKey* nextKey);
	Key* setResetKey(Key* targ);
	Key* setAlwaysReset(bool alwaysReset);
	virtual bool advanceKey();
	bool getAlwaysReset();
	BasicKey* getKey();
	BasicKey* getDefaultKey();
	BasicKey* getLastKey();

	BasicKey* getNext(bool wrap);
	BasicKey* getPrevious(bool wrap);

	int sequenceLength = 0;
	int getIndex();
	void resetKey();
	void resetTargetKeyReset();

	// this function is called when the key is pressed/released
	// this is for custom code execution on key press
	virtual void keyReleased() {};
	virtual void keyPressed() {};

	// if repeat is true, then sequence will start over
	// once the end is reached (manually)
	// otherwise sequence will play until the end and only
	// activate the ending key upon invocation

	// using a different key resets the index to 0
	// using this same key will manually advance the index
	// when the advance duration is -1

	bool state = false;
	int bounce = 0;
	const char* title = nullptr;
	Key* resetTargetKey = nullptr;
protected:
	int currentIndex = 0;
	bool repeat = false;
	bool alwaysReset = false;
	BasicKey* defaultKey = nullptr;
	BasicKey* currentKey = nullptr;
	BasicKey* lastKey = nullptr;
	void cacheSequenceLength();
};

class Profile {

public:
	Profile(const char * title, int col, Key** lut, int dimx, int dimy);
	const char * getTitle();
	Key* getKey(int x, int y);
	Key* getExtra(int i);
	int getDimy();
	int getDimx();
	int getR();
	int getG();
	int getB();

private:
	const char* title;
	int col = ~0;
	int dimx;
	int dimy;
	Key** lut;

};

class KritaRotaryKnobKey :public Key {
public:
	KritaRotaryKnobKey(DBounce* switchTarget) :Key(nullptr, "OPACITY CTRL") {
		this->switchTarget = switchTarget;
	};
	//void keyPressed();
	DBounce* switchTarget = nullptr;
	void keyReleased();
};

class KritaBrushSizeKey : public Key {
public:
	KritaBrushSizeKey(BasicKey* key, const char* title, DBounce* switchTarget) : Key(key, title, true) {
		this->switchTarget = switchTarget;
	};
	DBounce* switchTarget = nullptr;
	bool advanceKey();
};