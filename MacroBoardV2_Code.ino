/*********************************************************************
  This is an example for our Monochrome OLEDs based on SSD1306 drivers

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/category/63_98

  This example is for a 128x64 size display using SPI to communicate
  4 or 5 pins are required to interface

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada  for Adafruit Industries.
  BSD license, check license.txt for more information
  All text above, and the splash screen must be included in any redistribution
*********************************************s***********************/

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <SD.h>
// #include <Adafruit_SSD1306.h>
#include "Modal.h"
// If using software SPI (the default case):
#define OLED_MOSI  11
#define OLED_CLK   14
#define OLED_DC    15
#define OLED_CS    10
#define OLED_RESET 9
Adafruit_SSD1306 display(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);
Adafruit_SSD1306* Mode::disp = &display;

/* Uncomment this block to use hardware SPI
  #define OLED_DC     6
  #define OLED_CS     7
  #define OLED_RESET  8
  Adafruit_SSD1306 display(OLED_DC, OLED_RESET, OLED_CS);
*/

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

// byte sw_ledPins[6] = {23, 22, 21, 20, 19, 18};
// byte but_ledPins[3] = {17, 16, 39};
// byte colPins[7] = {4, 5, 6, 24, 25, 26, 27};
// byte rowPins[5] = {28, 29, 30, 31, 32};
// byte sw_pins[8] = {38, 37, 36, 35, 34, 33, 2, 7};
DBounce* sw_dbounce[8] = {
  new DBounce(0, 0),
  new DBounce(0, 0),
  new DBounce(0, 0),
  new DBounce(0, 0),
  new DBounce(0, 0),
  new DBounce(0, 0),
  new DBounce(0, 0),
  new DBounce(0, 0)
};

byte enc_outA = 0;
byte enc_outB = 1;
byte enc_sw1 = 2;
byte enc_sw2 = 3;

byte enc_pos = 0;
byte enc_lastPos = 0;
int cycle = 0;
int hue = 0;
int selected = 0;

const int bounceCount = 255;

const int dimx = 7;
const int dimy = 5;

//0b[shift][ctrl][alt]
Key* lut2[ dimx * dimy + 3] = {new Key(KEY_DELETE, 0b000,"DELETE"), new Key('a', 0b000), new Key('b', 0b000), new Key('c', 0b000), new Key('d', 0b000), new Key('e', 0b000), new Key(KEY_BACKSPACE, 0b000,"BACKSPACE"),
  new Key('f', 0b000), new Key('g', 0b000), new Key('h', 0b000), new Key('i', 0b000), new Key('j', 0b000), new Key('k', 0b000), new Key('l', 0b000),
  new Key('m', 0b000), new Key('n', 0b000), new Key('o', 0b000), new Key('p', 0b000), new Key('q', 0b000), new Key('r', 0b000), new Key('s', 0b000),
  new Key('t', 0b000), new Key('u', 0b000), new Key('v', 0b000), new Key('w', 0b000), new Key('x', 0b000), new Key('y', 0b000), new Key('z', 0b000),
  new Key('|', 0b000), new Key(KEY_LEFT_SHIFT, 0b000, "SHIFT"), new Key(KEY_LEFT_CTRL, 0b000,"CTRL"), new Key(KEY_LEFT_ALT, 0b000,"ALT"), new Key(0x20, 0b000,"SPACE"), new Key(KEY_ENTER, 0b000,"ENTER"), new Key('|', 0b000),
  new Key(KEY_DOWN_ARROW,0b000,"UP ARROW"), new Key(KEY_UP_ARROW,0b000,"DOWN ARROW"), new Key('a',0b010,"SELECT ALL")
};
Key* key_transform = new Key(new BasicKey('t',0b010, -1, new BasicKey(KEY_ESC,0b000)), "TRANSFORM", true);

Key* lut[dimx * dimy + 3] = {new Key('a', 0b000), new Key('b', 0b000), new Key('c', 0b000), new Key('d', 0b000), new Key('f', 0b000), new Key('s', 0b000), new Key('n', 0b000),
  new Key(KEY_F12, 0b110, "SAVE MACRO"), new Key(KEY_LEFT_SHIFT, 0b000), new Key(KEY_LEFT_CTRL, 0b000), new Key(KEY_LEFT_ALT, 0b000), new Key('x', 0b000, "SWAP FG BG COL"), new Key('d', 0b010, "DESELECT"), new Key(KEY_DELETE, 0b000, "DELETE"),
  new Key(KEY_ENTER, 0b000, "ENTER"), new Key('o', 0b000), new Key('e', 0b000, "ERASER"), new Key('b', 0b000, "BRUSH"), new Key('k', 0b000, "COLOR PICKER"), new Key('=', 0b110, "FLIP H"), new Key('a', 0b010, "SELECT ALL"),
  new Key('=', 0b010,"ZOOM IN"), new Key('j', 0b010, "DUPLICATE LAYER"), new Key('m', 0b000, "MARQUEE"), new Key(0x20, 0b000, "PAN"), new Key('l', 0b000, "LASSO"), key_transform, new Key('}', 0b000,"HARDNESS +"),
  new Key('|', 0b000), new Key('-', 0b010, "ZOOM OUT"), new Key('z', 0b011,"UNDO"), new Key('r', 0b000,"ROTATE"), new Key('z', 0b110,"REDO"), new Key('{', 0b000,"HARDNESS -"), new Key('|', 0b000),
  new Key('[',0b000,"BRUSH SIZE -"), new Key(']',0b000,"BRUSH SIZE +"), new Key('a',0b010,"SELECT ALL")
};

Key* krita_eraser = new Key(new BasicKey('e', 0b000, -1, new BasicKey(-1,0b000)), "KRITA ERASER");
Key* krita_brush = new Key(new BasicKey('e', 0b000, 20, new BasicKey('b',0b000)), "KRITA BRUSH", krita_eraser);

Key* lut3[dimx * dimy + 3] = {new Key('a', 0b000), new Key('b', 0b000), new Key('c', 0b000), new Key('d', 0b000), new Key('f', 0b000), new Key('s', 0b000), new Key('n', 0b000),
  new Key('s', 0b010, "SAVE"), new Key(KEY_LEFT_SHIFT, 0b000), new Key(KEY_LEFT_CTRL, 0b000), new Key(KEY_LEFT_ALT, 0b000), new Key('x', 0b000, "SWAP FG BG COL"), new Key('d', 0b010, "DESELECT"), new Key(KEY_DELETE, 0b000, "DELETE"),
  new Key(KEY_ENTER, 0b000, "ENTER"), new Key('o', 0b000), krita_eraser, krita_brush, new Key('k', 0b000, "COLOR PICKER"), new Key('=', 0b010, "FLIP H"), new Key('a', 0b010, "SELECT ALL"),
  new Key('=', 0b110,"ZOOM IN"), new Key('j', 0b010, "DUPLICATE LAYER"), new Key('m', 0b000, "MARQUEE"), new Key(0x20, 0b000, "PAN"), new Key('j', 0b100, "OUTLINE"), key_transform, new Key('}', 0b000,"HARDNESS +"),
  new Key('|', 0b000), new Key('-', 0b010, "ZOOM OUT"), new Key('z', 0b010,"UNDO"), new Key('r', 0b000,"ROTATE"), new Key('z', 0b110,"REDO"), new Key('{', 0b000,"HARDNESS -"), new Key('|', 0b000),
  new Key('[',0b000,"BRUSH SIZE -"), new Key(']',0b000,"BRUSH SIZE +"), new Key('a',0b010,"SELECT ALL")
};

Profile p1("Photoshop", 0x0000FF, lut, dimx, dimy);
Profile p2("Generic", ~0x0, lut2, dimx, dimy);
Profile p3("Krita", 0x00FF00, lut3, dimx, dimy);
OperateMode photo(&p1);
OperateMode gen(&p2);
OperateMode krita(&p3);
const int numProfiles = 3;

Mode* sm2[numProfiles] = {&photo, &krita, &gen};
Mode keys("Profiles", sm2, numProfiles);

// SettingsMode settings;
Mode back("Back");
Mode* sm1[1] = {&keys};

Mode mainMode("Main Menu", sm1, 1);

Mode* current_mode = &mainMode;
Profile* current_profile = &p1;

void setup() {
  krita_eraser->setResetKey(krita_brush)->setAlwaysReset(true);
  key_transform->setAlwaysReset(true);
  Serial.begin(9600);

  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC);
  // init done
  display.display();
  delay(1000);

  // Show image buffer on the display hardware.
  // Since the buffer is intialized with an Adafruit splashscreen
  // internally, this will display the splashscreen.
  display.setRotation(2);
  display.clearDisplay();

  display.setTextSize(1);
  display.setTextColor(WHITE);

  if (!SD.begin(BUILTIN_SDCARD)) {
    display.clearDisplay();
    display.setCursor(2,12);
    display.print("SD Card Failed");
    display.display();
    return;
  }
  display.clearDisplay();
  display.setCursor(2,12);
  display.print("SD Card Initialized");
  display.setCursor(2,22);

  display.display();
  delay(1000);

  pinMode(enc_outA, INPUT_PULLUP);
  pinMode(enc_outB, INPUT_PULLUP);
  pinMode(enc_sw1, INPUT_PULLUP);
  pinMode(enc_sw2, OUTPUT);
  digitalWrite(enc_sw2, LOW);

  pinMode(13, OUTPUT);

  for (int a = 0; a < 8; a++) {
    if(a<6){
      pinMode(sw_ledPins[a], OUTPUT);
      analogWrite(sw_ledPins[a], 0);
    }
    pinMode(sw_pins[a], INPUT_PULLUP);
  }

  for (int a = 0; a < 3; a++) {
    pinMode(but_ledPins[a], OUTPUT);
    analogWrite(but_ledPins[a], 255);
  }

  for (int a = 0; a < dimx; a++) {
    pinMode(colPins[a], INPUT_PULLUP);
  }

  for (int a = 0; a < dimy; a++) {
    pinMode(rowPins[a], OUTPUT);
    digitalWrite(rowPins[a], HIGH);
  }

  Keyboard.begin();

}

void loop() {

  cycle++;
  if (cycle == 840) {
    cycle = 0;
    if (current_mode->needsUpdate()) {
      displayMode(current_mode);
      current_mode->setUpdate(false);
    }
  }

  for (int a = 0; a < 8; a++) {
    if ((!digitalRead(sw_pins[a]) && sw_dbounce[a]->bounce == 0) || (sw_dbounce[a]->bounce > 0 && sw_dbounce[a]->bounce < bounceCount)) {
      sw_dbounce[a]->bounce++;
    }
    if (sw_dbounce[a]->bounce == bounceCount) {
      if (!digitalRead(sw_pins[a])) {
        sw_dbounce[a]->state = 1 ^ sw_dbounce[a]->state;
        current_mode->switchState(a,sw_dbounce[a]);
      }
      sw_dbounce[a]->bounce++;
    }
    if (sw_dbounce[a]->bounce > bounceCount) {
      if (digitalRead(sw_pins[a])) {
        sw_dbounce[a]->bounce = 0;
      }
    }

    if (a < 6 && !sw_dbounce[a]->block) {
      digitalWrite(sw_ledPins[a], sw_dbounce[a]->state);
    }
  }

  if (sw_dbounce[3]->state > 0) {
    delay(100);
    sw_dbounce[3]->state = 0;
    toParentMode();
  }

  if (sw_dbounce[6]->state > 0) {
    sw_dbounce[6]->state = 0;
    selectMode();
  }

  if (sw_dbounce[7]->state > 0) {
    sw_dbounce[7]->state = 0;
  }

  digitalWriteFast(enc_outA, HIGH);
  digitalWriteFast(enc_outB, HIGH);
  bool enc_a = !digitalReadFast(enc_outA);
  bool enc_b = !digitalReadFast(enc_outB);
  enc_pos = 0;
  if (enc_a && !enc_b) {
    enc_pos = 1;
  }
  if (enc_a && enc_b) {
    enc_pos = 2;
  }
  if (!enc_a && enc_b) {
    enc_pos = 3;
  }
  int diff = enc_pos - enc_lastPos;
  enc_lastPos = enc_pos;
  if (diff == 1 || diff == -3) {
    current_mode->cycle(-1);
    //hue--;
  }
  if (diff == -1 || diff == 3) {
    current_mode->cycle(1);
    //hue++;
  }


  if (hue > 360) {
    hue = 0;
  }
  if (hue < 0) {
    hue = 360;
  }



  float r = 0;
  float g = 0;
  float b = 0;
  float h = (float)hue / 60.0;
  float x = 1 - abs(fmod(h, 2.0) - 1);

  if (h >= 0.0 && h < 1.0) {
    r = 1.0;
    g = x;
    b = 0.0;
  }
  if (h >= 1.0 && h < 2.0) {
    r = x;
    g = 1.0;
    b = 0.0;
  }
  if (h >= 2.0 && h < 3.0) {
    r = 0.0;
    g = 1.0;
    b = x;
  }
  if (h >= 3.0 && h < 4.0) {
    r = 0.0;
    g = x;
    b = 1.0;
  }
  if (h >= 4.0 && h < 5.0) {
    r = x;
    g = 0.0;
    b = 1.0;
  }
  if (h >= 5.0 && h <= 6.0) {
    r = 1.0;
    g = 0.0;
    b = x;
  }

  if(current_mode->isOperator()){
    r = current_mode->getProfile()->getR() / 255.0;
    g = current_mode->getProfile()->getG() / 255.0;
    b = current_mode->getProfile()->getB() / 255.0;
  } else {
    r = 1;
    g = 1;
    b = 1;
  }

  digitalWriteFast(16, (cycle % 20) < (r * 20)); // red
  digitalWriteFast(17, (cycle % 20) < (g * 20)); // green
  digitalWriteFast(39, (cycle % 20) < (b * 20)); // blue

  current_mode->execute();
}

void selectMode() {
  if (current_mode->hasChildren()) {
    current_mode = current_mode->getChild(current_mode->getSelected());
    current_mode->setUpdate(true);
  }
  current_mode->onSelect();
}
void toParentMode() {
  current_mode->onExit();
  if (current_mode->hasParent()) {
    current_mode = current_mode->getParent();
    current_mode->setUpdate(true);
  }
}

void displayMode(Mode* m) {
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(1);
  //display.setCursor(display.width()-24, 0);
  //display.print(hue);
  display.setCursor(0, 0);
  display.print(m->title);
  display.drawFastHLine(0, 9, display.width(), WHITE);
  if (m->hasChildren()) {
    display.fillRect(0, 10 + m->getSelected() * 10, display.width(), 11, WHITE);
    for (int a = 0; a < m->childCount() ; a++) {
      if (a == m->getSelected()) {
        display.setTextColor(BLACK, WHITE);
      } else {
        display.setTextColor(WHITE);
      }
      display.setCursor(4, 12 + (10 * a));
      display.print(m->getChild(a)->title);
    }
  }

  m->onDisplay(&display);

  display.display();
}
