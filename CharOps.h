#pragma once


bool isAlphaNum(unsigned char c) {
	return (c >= 48 && c <= 57) || (c>=65 && c<=90) || (c >= 97 && c <= 122);
}

unsigned char toUpper(unsigned char c) {
	unsigned char result = c;
	if (c >= 97 && c <= 122) {
		result = c - 0x20;
	}
	return result;
}
