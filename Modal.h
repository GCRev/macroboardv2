#pragma once
#include "PinAccess.h"
#include "Profile.h"
#include "Adafruit_SSD1306.h"
#include <SD.h>
typedef unsigned char byte;

class Mode {

public:
	static Adafruit_SSD1306* disp;

	Mode(const char* title);
	Mode(const char* title, Mode** subs, int subsLen);

	virtual void switchState(int sw, DBounce* state);

	virtual int cycle(int i);
	virtual void onSelect();
	virtual void onExit();
	int getSelected();
	void setSelected(int i);
	Mode* getParent();
	Mode* getChild(int i);
	void setParent(Mode* par);
	virtual bool hasChildren();
	bool hasParent();

	const char* title;
	void setChildren(Mode** subs, int len);
	int childCount();
	virtual void execute();
	virtual void onDisplay(Adafruit_SSD1306* disp);
	virtual bool needsUpdate();
	void setUpdate(bool flag);

	virtual bool isOperator();
	virtual Profile * getProfile();

private:
	Mode* parent = nullptr;
	Mode** children = nullptr;
	int numChildren = 0;
	int selected = 0;
	bool needs_update = true;

};

class OperateMode : public Mode {

public:
	OperateMode(Profile * profile);

	int bounceCount = 255;

	void execute();
	int cycle(int i);
	void switchState(int sw, DBounce* state);
	void onDisplay(Adafruit_SSD1306* disp);
	void setCurrentProfile(Profile* prof);
	void onSelect();
	void onExit();
	bool isOperator();
	Profile * getProfile();

private:
	bool begin = false;
	unsigned char mod = 0b000;
	int selectedProfile = 0;
	void sendKey(Key* k, bool rise);
	void sendKey(Key* k, bool rise, bool sequence);
	Profile * current_profile = nullptr;
	Key* last_key;
	bool preview_mode = false;
};

class ProfileMode : public Mode {

public:
	ProfileMode(Profile* prof);
private:
	Profile* target_profile = nullptr;
};

class SettingsMode : public Mode {
public:
	File root;
	SettingsMode();
	void onDisplay(Adafruit_SSD1306* disp);
	void execute();
	void onSelect();
	int cycle(int i);
private:
	int fileIndex = 0;
	int startIndex = 0;
	int numFiles = 0;
	int maxFiles = 6;
	void printDirectory(File dir, Adafruit_SSD1306* disp);
};

class ProfileLoaderMode : public Mode {

public:
	ProfileLoaderMode(File f);
	Profile* getProfile();
private:
	Profile* profile;
	File* targetFile;
	void generateProfile();
};